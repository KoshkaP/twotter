import { createStore } from 'vuex';
import { UserModule } from './User';

export default createStore({
  modules: {
    User: UserModule
  }
})
